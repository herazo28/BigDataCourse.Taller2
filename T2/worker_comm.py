# -*- coding: utf-8 -*-
"""
Created on Tue Mar 28 12:08:00 2017

@author: omar
"""

from pexpect import pxssh
import time

def sendPetition(nombrePersona, pais, ciudad, fechaInicio, fechaFin):
    s = pxssh.pxssh()
    if not s.login ('172.24.99.76', 'bigdata8', '3955bcfee9a80c776277be5a62f962e6'):
        print("SSH session failed on login.")
        print(str(s))
    else:
        s.sendline('hadoop fs -rm -r test2_output2')
        s.prompt()
        s.sendline('cd Test2')
        s.prompt()
        s.sendline('rm nohup.out')
        s.prompt()
        #comando = 'nohup hadoop jar FilterWiki.jar uniandes.reuters.job.FilterWiki /datos/wiki/* test2_output2 ' + nombrePersona + ' &'
        comando = 'nohup hadoop jar FilterWiki1.jar uniandes.reuters.job.FilterWiki /datos/wiki/* test2_output2 ' + nombrePersona + ' ' + pais + ' ' + ciudad + ' ' + fechaInicio + ' ' + fechaFin + ' &'
        print(comando)
        s.sendline(comando)
                
        s.prompt()
        s.sendline('hadoop fs -cat test2_output2/part-r-00000')
        s.prompt()
        a = s.before
        
        while (a.decode("utf-8").find("No such file or directory\r\n") >= 0):
            print("1")
            time.sleep(20)
            s.sendline('hadoop fs -cat test2_output2/part-r-00000')
            s.prompt()
            a = s.before
            
        s.logout()    
        cantNodes = 0
        listNodes = []
        listEdges = []
        dictNodes = {}
        for tripleta in a.decode("utf-8").split('\r\n')[1:-2]:
            persona = tripleta.split('\t')[0]
            
            if persona in dictNodes:
                idOrigen = dictNodes[persona]
            else:
                cantNodes = cantNodes + 1
                idOrigen = cantNodes
                dictNodes[persona] = idOrigen
                listNodes.append({"id": idOrigen, "label": persona})
            
            print('persona:' + persona)
            try:
                for relacion in tripleta.split('\t')[1].split('&')[:-1]:
                    objeto = relacion.split('*')[1]
                    if objeto in dictNodes:
                        idDestino = dictNodes[objeto]
                    else:
                        cantNodes = cantNodes + 1
                        idDestino = cantNodes
                        dictNodes[objeto] = idDestino  
                        listNodes.append({"id": idDestino, "label": objeto})
                    
                    accion = relacion.split('*')[0]
                    listEdges.append({"from": idOrigen, "to": idDestino, "label": accion, "font": {"align": "middle"}, "arrows":"to"})
                    
                    print('accion: ' + accion)
                    print('objeto: ' + objeto)
            except:
                pass
            
            print("")
        resultado = {}
        resultado['nodes'] = listNodes
        resultado['edges'] = listEdges
        
        return resultado
        
        #print(s.before)
def getDefault():
    s = pxssh.pxssh()
    if not s.login ('172.24.99.76', 'bigdata8', '3955bcfee9a80c776277be5a62f962e6'):
        print("SSH session failed on login.")
        print(str(s))
    else:
        s.sendline('hadoop fs -cat default/part-r-00000')
        s.prompt()
        a = s.before
        s.logout() 
        
        cantNodes = 0
        listNodes = []
        listEdges = []
        dictNodes = {}
        for tripleta in a.decode("utf-8").split('\r\n')[1:-2]:
            persona = tripleta.split('\t')[0]
            
            if persona in dictNodes:
                idOrigen = dictNodes[persona]
            else:
                cantNodes = cantNodes + 1
                idOrigen = cantNodes
                dictNodes[persona] = idOrigen
                listNodes.append({"id": idOrigen, "label": persona})
            
            print('persona:' + persona)
            try:
                for relacion in tripleta.split('\t')[1].split('&')[:-1]:
                    objeto = relacion.split('*')[1]
                    if objeto in dictNodes:
                        idDestino = dictNodes[objeto]
                    else:
                        cantNodes = cantNodes + 1
                        idDestino = cantNodes
                        dictNodes[objeto] = idDestino  
                        listNodes.append({"id": idDestino, "label": objeto})
                    
                    accion = relacion.split('*')[0]
                    listEdges.append({"from": idOrigen, "to": idDestino, "label": accion, "font": {"align": "middle"}, "arrows":"to"})
                    
                    print('accion: ' + accion)
                    print('objeto: ' + objeto)
            except:
                pass
            
            print("")
        resultado = {}
        resultado['nodes'] = listNodes
        resultado['edges'] = listEdges
        
        return resultado
#getRelations(texto)
#sendPetition('Paul')