from flask import Flask, request
from flask import render_template

import worker_comm

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('test.html')

@app.route('/punto1')
def punto1():
    try:
        pais = request.args.get('pais')
        if pais == '':
            pais = 'NULL'
    except:
        pais = 'NULL'
    try:
        ciudad = request.args.get('ciudad')
        if ciudad == '':
            ciudad = 'NULL'
    except:
        ciudad = 'NULL'
    try:
        personaje = request.args.get('personaje')
        if personaje == '':
            personaje = 'NULL'
    except:
        personaje = 'NULL'
    try:   
        fechaInicio = request.args.get('fechaInicio')
        if fechaInicio == '':
            fechaInicio = '01/01/1900'
    except:
        fechaInicio = '01/01/1900'
        #fechaInicio = fechaInicio.split('/')[1] + '/' + fechaInicio.split('/')[0] + '/' + fechaInicio.split('/')[2]
    try:
        fechaFin = request.args.get('fechaFin')
        if fechaFin == '':
            fechaFin = '31/12/2100'
        #fechaFin = fechaFin.split('/')[1] + '/' + fechaFin.split('/')[0] + '/' + fechaFin.split('/')[2]
    except:
        fechaFin = '31/12/2100'  
    
    resp = str(worker_comm.sendPetition(personaje, pais, ciudad, fechaInicio, fechaFin)).replace("'", '"')
    print(resp)
    return resp
    
@app.route('/default')
def default():
    print('HOLAAA')
    resp = str(worker_comm.getDefault()).replace("'", '"')
    print(resp)
    return resp


    
@app.route('/punto2', methods=['GET', 'POST'])
def punto2():
    if request.method == 'POST':
        return 'Hola Mundo!'
    else:
        return render_template('punto1.html')

app.run(debug=True, host ='0.0.0.0', port=8081)
