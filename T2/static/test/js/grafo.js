function obtenerNodos()
{
    document.getElementById('divLoading').style.display = "block";
    pais = document.getElementById('txtPais').value;
    ciudad = document.getElementById('txtCiudad').value;
    personaje = document.getElementById('txtPersonaje').value;
    fechaInicio = document.getElementById('txtFechaInicio').value;
    fechaFin = document.getElementById('txtFechaFin').value;

    $.get( "http://172.24.101.148:8081/punto1?pais=" + pais + "&ciudad=" + ciudad + "&personaje=" + personaje + "&fechaInicio=" + fechaInicio + "&fechaFin=" + fechaFin, function(data) {
      
      data = $.parseJSON(data);

      var nodes = new vis.DataSet(data['nodes']);
      var edges = new vis.DataSet(data['edges']);
      var container = document.getElementById('mynetwork');
      
      var data = {
            nodes: data['nodes'],
            edges: data['edges']
        };
        var options = {
            nodes: {
              shape: 'dot',
              scaling:{
                label: {
                  min:8,
                  max:20
                }
              }
            }
        };
        var network = new vis.Network(container, data, options);
        document.getElementById('divLoading').style.display = "none";
    })
    
}

function obtenerDefault()
{
    document.getElementById('divLoading').style.display = "block";
    $.get( "http://172.24.101.148:8081/default", function(data) {
      
      data = $.parseJSON(data);

      var nodes = new vis.DataSet(data['nodes']);
      var edges = new vis.DataSet(data['edges']);
      var container = document.getElementById('mynetwork');
      
      var data = {
            nodes: data['nodes'],
            edges: data['edges']
        };
        var options = {
            nodes: {
              shape: 'dot',
              scaling:{
                label: {
                  min:8,
                  max:20
                }
              }
            }
        };
        var network = new vis.Network(container, data, options);
        document.getElementById('divLoading').style.display = "none";
    })

}